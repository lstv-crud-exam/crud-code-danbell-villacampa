CREATE DATABASE  IF NOT EXISTS `employeedb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `employeedb`;
-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: localhost    Database: employeedb
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employeefile`
--

DROP TABLE IF EXISTS `employeefile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employeefile` (
  `recid` bigint NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `age` int DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `civilstat` varchar(45) DEFAULT NULL,
  `contactnum` varchar(45) DEFAULT NULL,
  `salary` decimal(10,0) DEFAULT NULL,
  `isactive` tinyint DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updateAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`recid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeefile`
--

LOCK TABLES `employeefile` WRITE;
/*!40000 ALTER TABLE `employeefile` DISABLE KEYS */;
INSERT INTO `employeefile` VALUES (2,'Danbell Martinez','Valenzuela City','1969-12-25',69,'female','widowed','09656650634',50000,0,'2023-03-31 17:25:21','2023-03-31 17:25:21'),(3,'Jhon Mark Dominguez','Malabon City','1998-02-11',24,'male','single','09656650634',10000,0,'2023-03-31 17:39:25','2023-03-31 17:39:25'),(4,'Bea Reyes','Malabon City','2023-03-01',56,'male','single','09656650634',100,1,'2023-03-31 17:59:56','2023-03-31 17:59:56'),(5,'Ruben Martinez','Navotas City','2023-03-01',56,'male','single','09656650634',100,1,'2023-03-31 18:00:56','2023-03-31 18:00:56'),(6,'Danbell Villacampa','Malabon City','1998-02-11',24,'male','single','09656650634',10000,1,'2023-03-31 18:03:28','2023-03-31 18:03:28'),(7,'Danbell Martinez Villacampa','Valenzuela City','2023-03-01',56,'male','single','09656650634',100,1,'2023-03-31 18:04:00','2023-03-31 18:04:00'),(8,'Ethan Tucay','Bulacan','1998-11-02',6,'female','separated','09656650634',1000000,1,'2023-03-31 18:04:42','2023-03-31 18:04:42'),(9,'Resty Locaberte','Pangasinan','1988-02-16',56,'female','separated','09656650634',100,1,'2023-03-31 18:06:33','2023-03-31 18:06:33'),(10,'Wilbert Caserio','Pampanga','1988-02-16',56,'female','separated','09656650634',100,1,'2023-03-31 18:06:54','2023-03-31 18:06:54'),(11,'Chen Elispasua','Navotas','1998-11-02',9,'other','separated','09656650634',200,1,'2023-04-01 13:28:52','2023-04-01 13:28:52'),(12,'Danbell Martinez Villacampa','blk 121 lot 16 phase 4 nhv, norzagaray','1987-05-09',56,'female','married','09656650634',100,1,'2023-04-01 13:37:21','2023-04-01 13:37:21'),(13,'Daniela Aguilando','Malabon','1998-05-06',56,'female','married','09656650634',1000000,0,'2023-04-01 18:10:27','2023-04-01 18:10:27');
/*!40000 ALTER TABLE `employeefile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-04-03 16:16:58

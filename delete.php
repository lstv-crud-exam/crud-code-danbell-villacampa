<?php

session_start();
require("connection.php");
if(!isset($_SESSION['email']) && !isset($_SESSION['password'])){
    header('location:./index.php');
    die();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Delete LSTV-CRUD Exam</title>
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="navigation container-fluid">
          <nav class="navbar navbar-light nav nav-pills nav-justified p-3" style="background-color: #e3f2fd;">
            <a class="nav-link" href="./create.php">CREATE</a>
            <a class="nav-link" href="./read.php">READ</a>
            <a class="nav-link" href="./update.php">UPDATE</a>
            <a class="nav-link active" aria-current="page" href="#">DELETE</a>
            <a class="nav-link" href="./index.php">LOGOUT</a>
        </nav>
    </div>
    <main class="container-fluid">
        <?php if(!isset($_POST['delete'])){ ?>
        <h1 class="text-center my-4">Delete Employee's Data</h1>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col" class="text-center">Record ID</th>
                    <th scope="col" class="text-center">Full Name</th>
                    <th scope="col" class="text-center">Address</th>
                    <th scope="col" class="text-center">Birthdate</th>
                    <th scope="col" class="text-center">Age</th>
                    <th scope="col" class="text-center">Gender</th>
                    <th scope="col" class="text-center">Civil Status</th>
                    <th scope="col" class="text-center">Contact Number</th>
                    <th scope="col" class="text-center">Salary</th>
                    <th scope="col" class="text-center">Active</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php   $employeeData = fetch_all("SELECT * FROM employeedb.employeefile"); 
                    foreach($employeeData as $empData){?>
                <tr>
                    <td class="text-center"><?= $empData['recid'] ?></td>
                    <td class="text-center"><?= $empData['fullname'] ?></td>
                    <td class="text-center"><?= $empData['address'] ?></td>
                    <td class="text-center"><?= $empData['birthdate'] ?></td>
                    <td class="text-center"><?= $empData['age'] ?></td>
                    <td class="text-center"><?= $empData['gender'] ?></td>
                    <td class="text-center"><?= $empData['civilstat'] ?></td>
                    <td class="text-center"><?= $empData['contactnum'] ?></td>
                    <td class="text-center"><?= $empData['salary'] ?></td>
                    <?php if($empData['isactive'] === "1"){
                    echo '<td class="text-center">True</td>';
                    }else{ 
                    echo '<td class="text-center">False</td>';
                    } ?>
                    <td><form method="POST">
                        <input type="hidden" name="delete" value="<?= $empData['recid'] ?>">
                        <button type="submit" class="btn btn-danger">DELETE</button>
                    </form></td>
                </tr>
            <?php } ?>    
            </tbody>
        </table>   
        <?php } else{
            $deleteEmployee = fetch_all("SELECT * FROM employeedb.employeefile WHERE recid = ".$_POST['delete']."");
            foreach($deleteEmployee as $deleteEmp){?>
        <h1 class="text-center mb-4 mt-4">Update</h1>
        <form action="process.php" method="POST">
            <!-- Start of Full Name -->
            <div class="mb-3">
                <label for="fullName" class="form-label">Full Name:</label>
                <input type="text" class="form-control" id="fullName" name="fullName" value="<?= $deleteEmp['fullname'] ?>" required>
            </div>
            <!-- End of Full Name -->
            <!-- Start of Address -->
            <div class="mb-3">
                <label for="address" class="form-label">Address: </label>
                <input type="text" class="form-control" id="address" name="address" value="<?= $deleteEmp['address'] ?>">
            </div>
            <!-- End of Address -->
            <!-- Start of Birthdate -->
            <div class="mb-3">
                <label for="birthdate" class="form-label">Birthdate: </label>
                <input type="date" class="form-control" id="birthdate" name="birthdate" value="<?= $deleteEmp['birthdate'] ?>">
            </div>
            <!-- End of Birthdate -->
            <!-- Start of Age -->
            <div class="mb-3">
                <label for="age" class="form-label">Age: </label>
                <input type="number" class="form-control" id="age" name="age" value="<?= $deleteEmp['age'] ?>">
            </div>
            <!-- End of Age -->
            <!-- Start of Gender -->
            <label class="mb-2">Gender:</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="male" value="male" 
                <?php 
                    if($deleteEmp['gender'] === 'male'){ 
                    echo "checked";  
                    } ?>
                >
                <label class="form-check-label" for="male">
                    Male
                </label>
            </div>

            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="female" value="female"  
                <?php 
                    if($deleteEmp['gender'] === 'female'){ 
                        echo "checked";  
                    } ?>
                >
                <label class="form-check-label" for="female">
                   Female
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="other" value="other" 
                <?php 
                    if($deleteEmp['gender'] === 'other'){ 
                        echo "checked";  
                    } ?>
                >
                <label class="form-check-label" for="other">
                   Other
                </label>
            </div>
            <!-- End of Gender -->
            <!-- Start of Civil Status -->
            <label class="mt-3 mb-2"> Civil Status:</label>
            <select class="form-select mb-3" name="civilstat" aria-label="Default select example">
                <option disabled>Choose your civil status</option>
                <option value="single"  
                <?php 
                    if($deleteEmp['civilstat'] === 'single'){ 
                        echo "selected";  
                    } ?> >Single</option>
                <option value="married"
                <?php 
                    if($deleteEmp['civilstat'] === 'married'){ 
                        echo "selected";  
                    } ?> >Married</option>
                <option value="separated" 
                <?php 
                    if($deleteEmp['civilstat'] === 'separated'){ 
                        echo "selected";  
                    } ?> >Separated</option>
                <option value="widowed"  
                <?php 
                    if($deleteEmp['civilstat'] === 'widowed'){ 
                        echo "selected";  
                    } ?> >Widowed</option>
            </select>
            <!-- End of Civil Status -->
            <!-- Start of Contact No. -->
            <div class="mb-3">
                <label for="contactnum" class="form-label">Contact No.: </label>
                <input type="tel" class="form-control" id="contactnum" name="contactnum" pattern="[0-9]{11}" value="<?= $deleteEmp['contactnum'] ?>">
            </div>
            <!-- End of Contact No. -->
            <!-- Start of Salary -->
            <div class="mb-3">
                <label for="salary" class="form-label">Salary: </label>
                <input type="text" inputmode="numeric" pattern="\d*" id="salary" name="salary" class="form-control" value="<?= $deleteEmp['salary'] ?>">
            </div>
            <!-- End of Salary -->
            <!-- Start of isActive -->
            <label class="mb-2" >Is the employee active?</label>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="1" id="active" name="active" 
                <?php 
                    if($deleteEmp['isactive'] === '1'){ 
                        echo "checked";  
                    } ?>
                >
                <label class="form-check-label" for="active">Active</label>
            </div>
            <!-- End of isActive -->
           
            <input type="hidden" name="delete" value="<?= $deleteEmp['recid']?>">
            <button type="submit" class="btn btn-danger mt-3 mb-5">Delete</button>
        </form>
        <?php   }
        } unset($_POST['delete']);  ?>
    </main>`

    <!-- Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>

<?php 
    session_start();
    if(!isset($_SESSION['email']) && !isset($_SESSION['password'])){
        header('location:./index.php');
        die();
    }
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login LSTV-CRUD Exam</title>
	<!-- Bootstrap -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <div class="navigation container-fluid">
        <nav class="navbar navbar-light nav nav-pills nav-justified p-3" style="background-color: #e3f2fd;">
            <a class="nav-link active" aria-current="page" href="./create.php">CREATE</a>
            <a class="nav-link" href="./read.php">READ</a>
            <a class="nav-link" href="./update.php">UPDATE</a>
            <a class="nav-link" href="./delete.php">DELETE</a>
            <a class="nav-link" href="./index.php">LOGOUT</a>
        </nav>
    </div>
	<div class="container">
        <h1 class="text-center my-4">Create Data</h1>
        <?php   
        if(!empty($_SESSION['error'])){
        ?>      
        <h3 class="text-center"><?= $_SESSION['error'] ?></h3>   
        <?php   } 
        unset($_SESSION['error']);
        ?>
        <form action="process.php" method="POST">
            <!-- Start of Full Name -->
            <div class="mb-3">
                <label for="fullName" class="form-label">Full Name:</label>
                <input type="text" class="form-control" id="fullName" name="fullName" required>
            </div>
            <!-- End of Full Name -->
            <!-- Start of Address -->
            <div class="mb-3">
                <label for="address" class="form-label">Address: </label>
                <input type="text" class="form-control" id="address" name="address">
            </div>
            <!-- End of Address -->
            <!-- Start of Birthdate -->
            <div class="mb-3">
                <label for="birthdate" class="form-label">Birthdate: </label>
                <input type="date" class="form-control" id="birthdate" name="birthdate">
            </div>
            <!-- End of Birthdate -->
            <!-- Start of Age -->
            <div class="mb-3">
                <label for="age" class="form-label">Age: </label>
                <input type="number" class="form-control" id="age" name="age">
            </div>
            <!-- End of Age -->
            <!-- Start of Gender -->
            <label class="mb-2">Gender:</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="male" value="male">
                <label class="form-check-label" for="male">
                    Male
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="female" value="female">
                <label class="form-check-label" for="female">
                   Female
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="other" value="other">
                <label class="form-check-label" for="other">
                   Other
                </label>
            </div>
            <!-- End of Gender -->
            <!-- Start of Civil Status -->
            <label class="mt-3 mb-2"> Civil Status:</label>
            <select class="form-select mb-3" name="civilstat" aria-label="Default select example">
                <option selected  disabled>Choose your civil status</option>
                <option value="single">Single</option>
                <option value="married">Married</option>
                <option value="separated">Separated</option>
                <option value="widowed">Widowed</option>
            </select>
            <!-- End of Civil Status -->
            <!-- Start of Contact No. -->
            <div class="mb-3">
                <label for="contactnum" class="form-label">Contact No.: </label>
                <input type="tel" class="form-control" id="contactnum" name="contactnum" pattern="[0-9]{11}">
            </div>
            <!-- End of Contact No. -->
            <!-- Start of Salary -->
            <div class="mb-3">
                <label for="salary" class="form-label">Salary: </label>
                <!-- <input type="text" class="form-control" id="salary" name="salary" pattern="[0-9]{}, [.]{1}"> -->
                <input type="text" inputmode="numeric" pattern="\d*" id="salary" name="salary" class="form-control"/>
            </div>
            <!-- End of Salary -->
            <!-- Start of isActive -->
            <label class="mb-2" >Is the employee active?</label>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="1" id="active" name="active">
                <label class="form-check-label" for="active">Active</label>
            </div>
            <!-- End of isActive -->
           
            <input type="hidden" name="create" value="create">
            <button type="submit" class="btn btn-primary mt-3 mb-5">Submit</button>
        </form>
    </div>

	<!-- Bootstrap -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
<?php

session_start();
require("connection.php");
if(!isset($_SESSION['email']) && !isset($_SESSION['password'])){
    header('location:./index.php');
    die();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home LSTV-CRUD Exam</title>
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="navigation container-fluid">
          <nav class="navbar navbar-light nav nav-pills nav-justified p-3" style="background-color: #e3f2fd;">
            <a class="nav-link" href="./create.php">CREATE</a>
            <a class="nav-link active" aria-current="page" href="#">READ</a>
            <a class="nav-link" href="./update.php">UPDATE</a>
            <a class="nav-link" href="./delete.php">DELETE</a>
            <a class="nav-link" href="./index.php">LOGOUT</a>
        </nav>
    </div>
    <main class="container-fluid">
        <h1 class="text-center my-4">Employees Data</h1>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col" class="text-center">Record ID</th>
                    <th scope="col" class="text-center">Full Name</th>
                    <th scope="col" class="text-center">Address</th>
                    <th scope="col" class="text-center">Birthdate</th>
                    <th scope="col" class="text-center">Age</th>
                    <th scope="col" class="text-center">Gender</th>
                    <th scope="col" class="text-center">Civil Status</th>
                    <th scope="col" class="text-center">Contact Number</th>
                    <th scope="col" class="text-center">Salary</th>
                    <th scope="col" class="text-center">Active</th>
                </tr>
            </thead>
            <tbody>
            <?php   $employeeData = fetch_all("SELECT * FROM employeedb.employeefile"); 
                    foreach($employeeData as $empData){?>
                <tr>
                    <td class="text-center"><?= $empData['recid'] ?></td>
                    <td class="text-center"><?= ucwords( $empData['fullname']) ?></td>
                    <td class="text-center"><?= ucwords($empData['address']) ?></td>
                    <td class="text-center"><?= $empData['birthdate'] ?></td>
                    <td class="text-center"><?= $empData['age'] ?></td>
                    <td class="text-center"><?= ucfirst($empData['gender']) ?></td>
                    <td class="text-center"><?= ucfirst($empData['civilstat']) ?></td>
                    <td class="text-center"><?= $empData['contactnum'] ?></td>
                    <td class="text-center"><?= $empData['salary'] ?></td>
                    <?php if($empData['isactive'] === "1"){
                    echo '<td class="text-center">True</td>';
                    }else{ 
                    echo '<td class="text-center">False</td>';
                    } ?>
                </tr>
            <?php } ?>    
            </tbody>
          </tbody>
        </table>
    </main>

    <!-- Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>